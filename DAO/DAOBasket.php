<?php

include ($_SERVER['DOCUMENT_ROOT'].'models/Fruit.php');
include ($_SERVER['DOCUMENT_ROOT'].'models/User.php');

class DAOBasket 
{
    private $pdo;

     public function __construct() {
        $dsn = 'mysql:dbname=Maison_du_fruit;host=127.0.0.1';
        $user = 'sg';
        $password = 'root';
        $this->pdo = new PDO($dsn,$user,$password);
    }

    /**
     * Méthode permettant la récupération de la liste de fruits sur la base de données. On requête la base de données grâce à l'objet PDO.
     * On retourne ensuite un array d'objets de type Fruit
     */

    public function getFruits() {
        $statement = $this->pdo->query('SELECT id, nom_fruit AS nomFruit, prix_ht AS prixHt FROM fruit');
        $datas = $statement->fetchAll(PDO::FETCH_CLASS, "Fruit");
        return $datas;
    }
    /**
     * Méthode permettant la récupération de la liste des utilisateurs sur la base de données. On requête la base de données grâce à l'objet PDO.
     * On retourne ensuite un array d'objets de type User
     */

    public function getUser() {
        $statement = $this->pdo->query('SELECT * FROM user');
        $datas = $statement->fetchAll(PDO::FETCH_CLASS, "User");
        return $datas;
    }
    /**
     * Méthode permettant d'inserer le panier passé en paramètre dans la base de données.
     * On insère des données dans la table panier et dans la table fruit_panier.
     * Pour cela on effectue une transaction.
     * On effectue un insert sur la table panier et on boucle sur la liste de fruit, pour chaque objet fruit on fait un insert dans la table fruit_panier. 
     */

    public function submitBasket($datas) {
        $fruits = $datas->getFruits();
        
        $idStatement = $this->pdo->query('SELECT MAX(id) FROM panier');
        $basketId = $idStatement->fetch();
        if ($basketId['MAX(id)']=== null) {
            $basketId['MAX(id)'] = 1;
        } else {
            $basketId['MAX(id)'] += 1; 
        }
        $this->pdo->beginTransaction();
        $statement = $this->pdo->prepare('INSERT INTO panier (id, date_transaction, total_ttc, total_ht, user_id) VALUES(:id, :date_transaction, :total_ttc, :total_ht, :user_id)');
        $result = $statement->execute(array(
            "id" => $basketId['MAX(id)'],
            "date_transaction" => $datas->getDateTransaction(),
            "total_ttc" => $datas->getTotalTtc(),
            "total_ht" => $datas->getTotalHt(),
            "user_id" => $datas->getUserId(),
        ));
        $fruitsStatement = $this->pdo->prepare('INSERT INTO fruit_panier (id_panier, id_fruit, quantite_fruit) VALUES(:id_panier, :id_fruit, :quantite_fruit)');
       
        foreach ($fruits as $fruit) {
        $fruitsStatement->bindParam(':id_panier', $basketId['MAX(id)']);
        $fruitsStatement->bindParam(':id_fruit', $fruit->getId());
        $fruitsStatement->bindParam(':quantite_fruit', $fruit->getQuantity());
        $fruitsStatement->execute();
         }
         $this->pdo->commit();
    }
}