<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <LINK href="style/style.css" rel="stylesheet" type="text/css">
    <title>Document</title>
</head>
<body>
<h1> La maison du fruit</h1>

<select id="fruitsValue" multiple>
    <?php foreach ($fruits as $fruit) {
         echo "<option value='".$fruit->getId()."#".$fruit->getNomFruit()."#".$fruit->getPrixHt()."'>".$fruit->getNomFruit()." (".$fruit->getPrixHt()."€)"."</option>";
     } ?>
</select>
<div>
<button id="add">Ajouter au panier</button>
    </div>
    <div class="col-25">
    <div class="container">
<table id="basket">
    <thead>
        <tr>
            <th class="columnTitle">Panier</th>
        </tr>
        <tr>
            <th class="columnTitle">Produits</th>
            <th>
            <th class="columnTitle">Prix</th>
    </tr>
    </thead>
    <tbody id="basketBody">
        <?php if($basket != null) {
         foreach ($basket as $article) {
            echo "<tr> <td class='".$article->getNomFruit()."'>".$article->getNomFruit()." X <span>".$article->getQuantity()."</span></td><td></td><td id='".$article->getNomFruit()."Prix'><span>".$article->price()."</span>€</td> </tr>";
        }
     } ?>
    </tbody>
    <tfoot>
        <tr>
            <th></th>
            <th></th>
            <th class="footerPrice" id="totalHt">Total HT : <span><?php if($currentBasket != null ){echo $currentBasket->priceWithoutTaxes();} ?></span> €</th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th class="footerPrice" id="totalTtc">Total TTC : <span><?php if($currentBasket != null ){echo $currentBasket->priceWithTaxes();} ?></span> € </th>
        </tr>
        <tr>
            <th></th>
            <th></th>
            <th class="footerPrice"><a class="btn" href="/submitBasket">Payer</a></th>
        </tr>            
    </tfoot>                            
    </table>
    </div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src ="./scripts/script.js"></script>
</html>


