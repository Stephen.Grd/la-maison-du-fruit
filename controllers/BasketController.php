<?php

include ($_SERVER['DOCUMENT_ROOT'].'DAO/DAOBasket.php');
include ($_SERVER['DOCUMENT_ROOT'].'models/Basket.php');

class BasketController 
{
    private $datas;

    public function __construct() {}
/**
 * Méthode permettant la récupération de la vue principale. Dans la méthode on récupére la liste des fruits,
 * le panier sauvegardé en session si il en existe un.
 */
    public function getView() {
            //Récupération de la liste des fruits via le DAO et stockage dans une variable $fruits
            $fruits = (new DAOBasket())->getFruits();
            // Si la session existe, on stocke le contenu de la session dans une variable $panier qui va nous permettre d'afficher le panier dans la vue
            if(isset($_SESSION['basket'])) { 
            $currentBasket = $_SESSION['basket'];
            $basket = $currentBasket->getFruits();
            } else {
            // Si la session n'existe pas, on créé une nouvelle session qui sera un objet de type Basket    
            $_SESSION['basket'] = new Basket();
            }    
        include($_SERVER['DOCUMENT_ROOT'].'basketView.php');
    }
    
/**
 * Méthode permettant l'ajout d'un article dans le panier. Dans la méthode on récupère les données de la superglobale $_POST
 * et on utilise la méthode addFruit de l'objet Basket. Méthode qui va ajouter le fruit ajouter dans la liste de fruits de l'objet Basket.
 * On echo ensuite les données de la superglobale post, au format JSON, dans le front. 
 */

    public function addArticle() {
        //
        $_SESSION['basket']->addFruit($_POST);
        echo json_encode($_POST);
        // header('Content-type: application/json');
        // echo json_encode($_SESSION['basket']);
    }

/**
 * Méthode permettant l'envoi du panier dans la base de données à la soumission de celui-ci.
 * Si l'objet panier en session n'est pas null on va récupérer la date actuelle, l'id de l'utilisateur puis on les affecter à notre objet basket en session 
 * On utilise ensuite la méthode submitBasket du DAO pour poster les données en base de données
 * On détruit la session pour vider le panier et on fait une redirection sur la racine du site web 
 */    

    public function submitBasket() {
        $date = date("Y-n-j");
        $user = new User();
        $user = (new DAOBasket())->getUser();
        $userId = $user[0]->getId();
        $_SESSION['basket']->setDateTransaction($date);
        $_SESSION['basket']->setUserId($userId);
        $_SESSION['basket']->priceWithoutTaxes();
        $_SESSION['basket']->priceWithTaxes();
        if($_SESSION['basket']->priceWithoutTaxes() != null) {
            (new DAOBasket())->submitBasket($_SESSION['basket']);
            session_destroy();
            header('Location: /');
        } 
    }
}