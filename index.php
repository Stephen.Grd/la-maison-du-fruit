<?php
include ($_SERVER['DOCUMENT_ROOT']."controllers/BasketController.php");
session_start();

$uri = $_SERVER['REQUEST_URI'];

switch($uri) {

    case "/" | "" : 

    $controller = new BasketController();
    $controller->getView();
    break;

    case "/addArticle" :

    $controller = new BasketController();
    $controller->addArticle();
    break;


    case "/submitBasket" :
    
    $controller = new BasketController();
    $controller->submitBasket();
    break;
}