<?php

class Basket 
{
    private $id;
    private $dateTransaction;
    private $totalTtc;
    private $totalHt;
    private $userId;
    private $fruits = [];

    /**
     * Méthode permettant d'ajouter un objet fruit, passé en paramètre de la méthode, à la liste d'objet $fruits.
     * On créé un objet de type Fruit avec les datas passées en paramètre.
     * On vérifie ensuite si la liste est vide, si la liste est vide ( propriété array $fruits) on incrémente la quantité de l'objet fruit grâce à sa méthode UpQuantity()
     * et on push l'objet fruit nouvellement créé dans la propriété array $fruits.
     * Sinon on boucle dans la liste, si l'objet existe déjà on incrémente juste sa quantité.
     * Si il n'existe pas on incrémente sa quantité et on le push dans la propriété array $fruits.
     * 
     */

    public function addFruit($fruit){
        $f = new Fruit($fruit);
        if (count($this->fruits) === 0) {
            $f->upQuantity();
            array_push($this->fruits,$f);
        } else {
        foreach($this->fruits as $key => $value) {
            if ($f->getId() === $value->getId()) {
                $this->fruits[$key]->upQuantity();
                return;
            }
        }
        $f->upQuantity();
        array_push($this->fruits,$f);
        }
    }

    /**
     * Méthode permettant le calcul du prix hors taxe du panier. On va boucler avec une foreach dans l'array $fruits et pour chaque objet fruit
     * on récupére sa quantité et son prix hors taxe qu'on multiple, on additionne le résultat de chaque objet, on affecte la valeur à la propriété totalHt et on retourne la propriété totalHt
     */

    public function priceWithoutTaxes() {
        $this->totalHt = 0;
        foreach($this->fruits as $key => $value) {
            $this->totalHt += ($value->getQuantity()) * ($value->getPrixHt());
        }
        return $this->totalHt;
    }
    /**
     * Méthode permettant le calcul du prix toutes taxes comprises du panier. On récupére la valeur de la propriété totalHt
     * et on multiple la valeur de totalHt par 1.20 étant donné que le montant des taxes est égal à 20%.
     * On affecte la valeur à la propriété totalTtc et on retourne la propriété.
     */
    public function priceWithTaxes() {
        $this->totalTtc = $this->totalHt * 1.20;
        return $this->totalTtc;

    }

    /**
     * Get the value of fruits
     */ 
    public function getFruits()
    {
        return $this->fruits;
    }

    /**
     * Set the value of fruits
     *
     * @return  self
     */ 
    public function setFruits($fruits)
    {
        $this->fruits = $fruits;

        return $this;
    }

    /**
     * Get the value of dateTransaction
     */ 
    public function getDateTransaction()
    {
        return $this->dateTransaction;
    }

    /**
     * Set the value of dateTransaction
     *
     * @return  self
     */ 
    public function setDateTransaction($dateTransaction)
    {
        $this->dateTransaction = $dateTransaction;

        return $this;
    }

    /**
     * Get the value of userId
     */ 
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Set the value of userId
     *
     * @return  self
     */ 
    public function setUserId($userId)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of totalHt
     */ 
    public function getTotalHt()
    {
        return $this->totalHt;
    }

    /**
     * Set the value of totalHt
     *
     * @return  self
     */ 
    public function setTotalHt($totalHt)
    {
        $this->totalHt = $totalHt;

        return $this;
    }

    /**
     * Get the value of totalTtc
     */ 
    public function getTotalTtc()
    {
        return $this->totalTtc;
    }

    /**
     * Set the value of totalTtc
     *
     * @return  self
     */ 
    public function setTotalTtc($totalTtc)
    {
        $this->totalTtc = $totalTtc;

        return $this;
    }
}