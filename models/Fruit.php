<?php

class Fruit 
{
    private $id;
    private $nomFruit;
    private $prixHt;
    private $quantite;

    public function __construct($f=null){
        if(!is_null($f)){
            $this->id = $f['idFruit'];
            $this->nomFruit = $f['nomFruit'];
            $this->prixHt = $f['prixHt'];
        }
        $this->quantite = 0;
        
    }
    /**
     * Méthode permettant d'incrémenter la propriété quantité. Utilisée lors de l'ajout d'un nouveau fruit dans le panier.
     */
    function upQuantity(){
        $this->quantite ++;
    }
    /**
     * Méthode permettant de calculer le prix d'un fruit en fonction de sa quantité. 
     */
    function price(){
        return ($this->prixHt * $this->quantite);
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of id
     *
     * @return  self
     */ 
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get the value of nomFruit
     */ 
    public function getNomFruit()
    {
        return $this->nomFruit;
    }

    /**
     * Set the value of nomFruit
     *
     * @return  self
     */ 
    public function setNomFruit($nomFruit)
    {
        $this->nomFruit = $nomFruit;

        return $this;
    }

    /**
     * Get the value of prixHt
     */ 
    public function getPrixHt()
    {
        return $this->prixHt;
    }

    /**
     * Set the value of prixHt
     *
     * @return  self
     */ 
    public function setPrixHt($prixHt)
    {
        $this->prixHt = $prixHt;

        return $this;
    }

    /**
     * Get the value of quantite
     */ 
    public function getQuantity()
    {
        return $this->quantite;
    }

    /**
     * Set the value of quantite
     *
     * @return  self
     */ 
    public function setQuantity($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }
}