/**
 * Fonction anonyme qui se déclenche lorsqu'on clique sur le bouton "Ajouter au panier".
 * La fonction va récupérer les valeurs présentes dans le select ( liste des fruits ) et va les formater.
 * Si il n'y aucune valeur dans le select on retourne une erreur.
 * Sinon on transforme la chaîne de caractère en tableau et on affecte les différentes valeurs récupérées dans des variables.
 * On effectue ensuite une requête Ajax de type POST sur l'URI addArticle en passant les variables crées dans les data.
 * Si la requête est un succès on parse les datas retournées et on déclenche la fonction basketView avec les datas passées en paramètre.
 * Si la requête est un echec, on affiche un message d'erreur et on affiche l'erreur dans la console.
 */

$("#add").click((event) => {
    selectedValue = $('#fruitsValue').val();
    if (selectedValue.length === 0) {
        alert("Aucun produit sélectionné, veuillez sélectionner un produit");
    } else {
        result = selectedValue.toString().split("#");
        idFruit = result[0];
        nomFruit = result[1];
        prixHt = result[2];
        $.ajax({
            type: "POST",
            url: "http://maison-test.bwb/addArticle",
            data: {
                "idFruit": idFruit,
                "nomFruit": nomFruit,
                "prixHt": prixHt,
            },
            success: function (datas) {
                result = JSON.parse(datas);
                basketView(result);
            },
            error: function (error) {
                alert("Erreur lors de l'ajout de l'article");
                console.log(error);
            }
        });
    }
});

/**
 * Fonction qui va permettre l'affichage dynamique du panier lorsqu'on clique sur "Ajouter au panier".
 * La fonction va manipuler le DOM pour intéragir avec les valeurs présentes dans celui-ci.
 * On va donc incrémenter les quantités, faire les caculs de prix grâce à cette fonction. 
 */

function basketView(datas) {
    // Ici on vérifie si une class correspondant au nom du fruit, présent dans les datas, existe dans le DOM.   
    if ($("."+datas['nomFruit']+"").length > 0) {
    // Si la class existe, on va récupérer la valeur de la quantité présente dans cette class ( qui est l'affichage de notre fruit dans le panier) et on l'incrémente de 1    
        let value = $("."+datas['nomFruit']+"> span").text();
        value = parseFloat(value) +1;
         // Si la class existe, on va récupérer la valeur du prix présent dans cette class et on l'additionne avec le prix de notre fruit courant.    
        $("."+datas['nomFruit']+"> span").text(value);
        let price = $("#"+datas['nomFruit']+"Prix > span").text();
        price = parseFloat(price) + parseFloat(datas['prixHt']);
        $("#"+datas['nomFruit']+"Prix > span").text(price.toFixed(2));
        // Si la class existe, on va récupérer la valeur du total HT et on l'additionne avec le prix de notre fruit courant.  
        let totalHt = $("#totalHt > span").text();
        totalHt = parseFloat(totalHt) + parseFloat(datas['prixHt']);
        // Si la class existe, on va récupérer la valeur du total HT et du total TTC et on le multiple le total HT par 1.20 ( 20% ) et on remplace le total TTC par ce résultat 
        $("#totalHt > span").text(totalHt.toFixed(2));
        let totalTtc = parseFloat(totalHt * 1.20); 
        $("#totalTtc > span").text(totalTtc.toFixed(2));
    // Si la class correspondant au fruit n'existe pas, on fabrique la vue avec les bonnes classes et les bons id et on initialise les valeurs
    } else {
        $("#basketBody").append("<tr> <td class='"+datas['nomFruit']+"'>"+datas['nomFruit']+" X <span>1</span></td><td></td><td id='"+datas['nomFruit']+"Prix'><span>"+datas['prixHt']+"</span>€</td> </tr>");
        let totalHt = $("#totalHt > span").text();
        if(totalHt === "") {
            let price = parseFloat(datas['prixHt']);
            $("#totalHt > span").text(price.toFixed(2));
        } else {
            let price = parseFloat(totalHt)+parseFloat(datas['prixHt']);
            $("#totalHt > span").text(price.toFixed(2));
        }
        let totalTtc = $("#totalTtc > span").text();
        if(totalTtc === "") {
            let price = parseFloat(datas['prixHt'] * 1.20);
            $("#totalTtc > span").text(price.toFixed(2));
        } else {
            let price = parseFloat(totalTtc)+parseFloat(datas['prixHt']) * 1.20;
            $("#totalTtc > span").text(price.toFixed(2));
        }
    }
}